import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt

url_adress = "http://apmonitor.com/che263/uploads/Main/heart_rate.txt"
tablesWithValues = pd.read_csv(url_adress)
tableForGauuse = pd.read_dat()

def bpm(t,c0,c1,c2, c3):
    return c0 + c1 *t - c2 * np.exp(-c3 * t)

time = tablesWithValues['Time (sec)'].values
heartRate = tablesWithValues['Heart Rate (BPM)'].values


p0 = [100,0.01, 100, 0.01]
c, cov = curve_fit(bpm, time, heartRate, p0)

print('Wypisz optymalne parametry')
print(c)

yp = bpm(time ,c[0], c[1], c[2],c[3])
print ( 'R^2: ', str(r2_score(heartRate, yp)))

plt.figure()
plt.title('Wykres serca Paulinki przed anglieskim')
plt.plot(time/60.0, heartRate, 'r--', label = "Zmierzone")
plt.plot(time/60.0, yp, 'b--', label = "Obliczone")
plt.xlabel('Opis osi X, w sekundach')
plt.ylabel('Opis osi Y, w udzerzeniach')
plt.legend(loc='best')
plt.show()

