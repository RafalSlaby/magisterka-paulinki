import csv



def dat2csv():
    with open('gauss.csv') as dat_file, open('gauss2.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file)

        for line in dat_file:
            row = [field.strip() for field in line.split('  ')]
            if len(row) == 6 and row[3] and row[4]:
                csv_writer.writerow(row)

def normalizeCSV(file):
    with open('punktyHisto.csv') as file, open('normalizedFile.csv', 'w') as newFile:
        for line in file:
            newFile.write(line.replace(';', ','))


normalizeCSV('gauss.csv')

