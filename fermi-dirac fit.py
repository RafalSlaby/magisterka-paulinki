import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt
from scipy import asarray as ar,exp


def csvNormalizer(fileName):
    with open(fileName, 'r') as file:
        with open(fileName+"2", 'a') as newFile:
            for line in file:
                lineSplited = line.split("  ")
                newFile.write(lineSplited[0]+","+lineSplited[1])
                print(lineSplited)

def gauss(x, a, x0, sigma):
    return a * exp(-(x - x0) ** 2 / (2 * sigma ** 2))

def loadFile(fileName):
    tablesWithValues = pd.read_csv(fileName)
    return tablesWithValues

def generateGaussData(mu, sigma):
    s = np.random.normal(mu, sigma, 1000)
    count, bins, ignored = plt.hist(s, 30, density=True)
    plt.plot(bins, 1 / (sigma * np.sqrt(2 * np.pi)) *np.exp( - (bins - mu)**2 / (2 * sigma**2) ), color='r')
    plt.show()

def femi_dirac_fit():
    fileData = loadFile('punktyHisto.csv')
    x = fileData['x'].values
    y = fileData['y'].values

    n = len(y)  # the number of data
    mean = 0
    sigma = sum(x * (y - mean) ** 2) / n  # note this correction

    print('n:', n)
    print('mean: ', mean)
    print('sigma: ', sigma)

    popt, pcov = curve_fit(gauss, y, x, p0=[90000, mean, sigma])
    print('a: ', popt[0])
    print('x0: ', popt[1])
    print('sigma: ', popt[2])

    #tworzenie wykresu
    plt.figure()
    plt.title('Podwojne dopasowanie Fermiego-Diraca')
    plt.bar(x, y, facecolor='green')
    plt.plot(x, gauss(x, popt[0], popt[1],popt[2]), 'ro:', label='fit', color='blue')
    #plt.plot(x, yp, 'b--', label = "Obliczone")
    plt.xlabel('Time dif between A and B sites')
    plt.ylabel('Number of counts')
    plt.legend(loc='best')
    plt.show()

def gaussian_fit():
    data = pd.read_csv("gauss.csv2")
    #x = ar(range(10))
    #y = ar([0, 1, 2, 3, 4, 5, 4, 3, 2, 1])

    y = ar((data[' yValues']**2).to_list())
    x = ar((data['xValues']).to_list())

    n = len(x)  # the number of data
    mean = sum(x * y) / n  # note this correction
    sigma = sum(y * (x - mean) ** 2) / n  # note this correction

    popt, pcov = curve_fit(gauss, x, y, p0=[1, mean, sigma])

    plt.plot(x, y, 'b+:', label='data')
    plt.plot(x, gauss(x, *popt), 'ro:', label='fit')
    plt.legend()
    plt.title('Fig. 3 - Fit for Time Constant')
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.show()



#femi_dirac_fit()
gaussian_fit()

