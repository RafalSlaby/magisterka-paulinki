def bpm(t,c0,c1,c2, c3):
    return c0 + c1 *t - c2 * np.exp(-c3 * t)

def fermi_dirac(E, μ, T):
    return 1/(np.exp((E - μ)/T) + 1)

