import matplotlib.pyplot as plt
import numpy as np
import re
import random
from scipy.stats import norm
import matplotlib.mlab as mlab
import scipy.optimize
from scipy.optimize import curve_fit


def GaussianFit(x, A, beta, B, mu, sigma):
    return (A * np.exp(-x / beta) + B * np.exp(-1.0 * (x - mu) ** 2 / (2 * sigma ** 2)))


class HistogramPlotter:
    def __init__(self, AxMIN=-15, AxMAX=15, AyMIN=0, AyMAX=300000, liczbaBinowNaWykresie=100):
        self.numberOfBinsOnPlot = liczbaBinowNaWykresie
        self.xAxisMin = AxMIN
        self.xAxisMax = AxMAX
        self.yAxisMin = AyMIN
        self.yAxisMax = AyMAX
        self.listHistogramBars = []
        self.histogramValues = []
        for x in range(liczbaBinowNaWykresie):
            self.listHistogramBars.append(0)
        self.histogramBarsCount = 0
        self.theGreatestValue = 0
        self.theGreatestHistogramePeek = 0
        self.xValues = []

    def readFile(self, fileName):
        readedValues = []
        with open(fileName) as f:
            for line in f:
                temp = (re.findall('\(\d+\,\d+\)', line))
                if len(temp) > 0:
                    temp = (temp[0][1:-1].split(","))
                    temp[0], temp[1] = int(temp[0]), int(temp[1])
                    readedValues.append(temp)
        step = (self.xAxisMax - self.xAxisMin) / self.numberOfBinsOnPlot
        stepIterator = self.xAxisMin
        # Tworzenie listy z 0 na indexach na których wiemy ze wartość bin-u wynosi 0
        for element in readedValues:
            self.listHistogramBars[element[0]] = element[1]

        # Symulowaie tego ze silarski ruszyl koncem chuja
        for value in self.listHistogramBars:
            for x in range(value):
                self.histogramValues.append(stepIterator)
            stepIterator += step

        # Tworzenie listy x
        step = (self.xAxisMax - self.xAxisMin) / self.numberOfBinsOnPlot
        stepIterator = self.xAxisMin
        for x in range(self.numberOfBinsOnPlot):
            self.xValues.append(stepIterator)
            stepIterator += step

    def displayPlot(self):
        (mu, sigma) = norm.fit(self.histogramValues)
        print("mu: ", mu)
        print("sigma: ", sigma)

        self.histogramValues = np.asarray(self.histogramValues)
        print("histo values: ", self.histogramValues)

        # the histogram of the data
        n, bins, patches = plt.hist(self.histogramValues, 100)

        y = norm.pdf(bins, mu, sigma)
        plt.plot(bins, y * 1000000, 'r--', linewidth=2)

        plt.xlabel('Bars IDs')
        plt.ylabel('Bars value')
        plt.title('Histogram')
        # plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
        plt.axis([self.xAxisMin, self.xAxisMax, self.yAxisMin, self.yAxisMax])

        #plt.axis([self.xAxisMin, self.xAxisMax, self.yAxisMin, self.theGreatestValue * 1.1])
        plt.grid(True)
        plt.show()

    def display_some_shit(self):
        x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])
        y = [22.4155688819, 22.3936180362, 22.3177538001, 22.1924849792, 21.7721194577, 21.1590235248, 20.6670446864,
             20.4996957642, 20.4260953411, 20.3595072628, 20.3926201626, 20.6023149681, 21.1694961343, 22.1077417713,
             23.8270366414, 26.5355924353, 31.3179807276, 42.7871637946, 61.9639549412, 84.7710953311]

        # Define a gaussian function with offset
        def gaussian_func(x, a, x0, sigma, c):
            return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2)) + c

        initial_guess = [1, 20, 2, 0]
        popt, pcov = curve_fit(gaussian_func, x, y, p0=initial_guess)

        xplot = np.linspace(0, 30, 1000)
        plt.scatter(x, y)
        plt.plot(xplot, gaussian_func(xplot, *popt))

        plt.show()

plotter = HistogramPlotter()
plotter.readFile("Canvas_1.C")
#plotter.displayPlot()
plotter.display_some_shit()
